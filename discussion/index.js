console.log("hello!")

// Arithmetic Operators
/*
	+ sum or addition
	- subtraction
	* multiplication
	/ division
	% modulo (returns the remainder)

*/
let x = 45;
let y = 28;

let sum = x + y;
console.log("Result of addition:" + sum);

let difference = x - y;
console.log("Result of subtraction:" + difference);

let product = x * y;
console.log("Result of multiplication:" + product);

let quotient = x / y;
console.log("Result of division:" + quotient);

let mod = x % y;
console.log("Result of Modulo:" + mod);


// Assignment Operator
// Basic assignment operator (=) or the equal symbol
let assignmentNumber = 8;

// Arithmetic Assignment Operator
// Addition Assignment Operator symbol (+=)
// Subtraction Assignment Operator symbol (-=)
// Multiplication Assignment Operator symbol (*=)
// Division Assignment Operator symbol (/=)
// Modulo Assignment Operator symbol (%=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition operator:" + assignmentNumber);

assignmentNumber += 2;
console.log("Result of addition operator:" + assignmentNumber);


// ORDER OF PRECEDENCE (PEMDAS)
/*
	P - parenthesis ()
	E - Exponent **
	M - multiplication *
	D - division /
	A - Addition +
	S - subtraction -
*/

let mdas = 1 + 2 - 3 * 4 / 5
console.log("Result of mdas: " + mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log("Result of pemdas: " + pemdas);



// Increment and Decrement
	/*
		- Operators that add or subtract values by 1
		and re-assigns the value of the variable
		where the increment/decrement was applied to
	*/

	let z = 1;

	let increment = ++z; // increment = 1 + z
	
	//the value "z" is added by a value of one before returning the value and storing it in variable "increment"
	console.log("Pre-increment: " + increment);
	// result: increment - 2
	// the value of z was also increased even though we didn't implicitly specify any value reassignment

	console.log("Value of z: " + z);
	//result: z = 2

	// The value of z is returned and stored in the variable 'increment' then the value of z is increased by one
	increment = z++;
	// increment = 2 Plus 1
	console.log("Post-increment: "+ increment);
	//The value of z was increased again reassigning to value to 3
	console.log("Value of z: " + z);



	//DECREMENT
	// the value of z is decreased by a value of one before returning the value and storing it in the variable 'decrement'
	let decrement = --z
	console.log("Pre-decrement: " + decrement);
	console.log("Value of z: " + z);


	decrement = z--;
	console.log("Post-decrement: " + decrement);
	console.log("Value of z: " + z);

// Type Coercion
	/*
		Type coercion is the automatic or implicit conversion of values from one data type to another
		- this happens when operations are performed on different data types that would normally not possible and yield irregular results
		- values are automatically converted from one data type to another in order to resolve operations.
	*/
	let numA = '10';
	let numB = 12;

	/*
		adding/concatenation a string and a number will result in a string
	*/

	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);


	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);


	let numE = true + 1;
	console.log(numE); // result : 1 + 1 = 2
	/*
		The boolean 'true' is also associated with the value of 1
	*/

	let numF = false + 2;
	console.log(numF);



// Equality Operator
	/*
		- Checks whether the operands are equal or have the same content
		- attempts to CONVERT and COMPARE operands of different data types
	*/

	
	console.log(1 == 1); // result: true
	console.log(1 == 2); // result: false
	console.log(1 == '1'); // result: true

	// STRICT EQUALITY
		/*
			- usually used in passwords
			- uses 3 = signs ====
			- checkes whether operands are equal/have the same content
			- ALSO COMPARE the data types of 2 values
			- JavaScript is a loosely typed language meaning theat values of different data types can be stored in variables
			- in combination with type coercion, this sometimes create problems within our codes (e.g. Java, Typescript)
			- Strict equality operators are better to use in most cases to ensure that data types provided are correct.
		*/

	console.log(1 === 1); //result: true
	console.log(1 === "1"); //result: false

// Inequality Operator
	/*
		- checks whether the operands are not equal or have different content
		- attempts to CONVERT and COMPARE operands of different data types
	*/
	console.log(1 != 1); //result: false
	console.log(1 != 3); // result: true
	console.log(1 != "1"); //result: false

	// STRICT INEQUALITY
	/*
		- !==
		- checks whether the operatnds are not equal/ have the same content
		- attempts to COMPARES the data types of 2 values
	*/	

	console.log(1 !== 1); //result: false
	console.log(1 !== "1"); //result: true


// Relational Operator
/*
	- Some comparison operators check whether one value is greater than or less than the other value
	- Like in equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement
*/

	let a = 50;
	let b = 65;

	// GT or Greater Than (>) 

	let isGreaterThan = a > b;
	console.log(isGreaterThan); // false

	// LT or Less Than (<)
	let isLessThan = a < b;
	console.log(isLessThan); // true


	//GTE or Greater than or equal to (>=)
	let GTE = a >= b;
	console.log(GTE); // false

	//LTE or Less than or equal to (<=)
	let LTE = a <= b;
	console.log(LTE); // true

	let numStr = '30';
	console.log	(a > numStr); // true

	let str = 'twenty'
	console.log(b >= str); // false

// Logical Operator

	/*
		AND operator (&&)
		- returns TRUE of all operands are true

		p 		q 			p && q
		false 	false 		false
		false	true		false
		true 	false		false
		true	true		true


		OR operator (|| - double pipe symbol)
		- return TRUE if one of the operands are true
		p 		q 				p || q
		false 	false			false
		false 	true			true
		true	false			true
		true	true			true
	*/

	let isAdmin	 = false;
	let isRegistered = true;
	let isLegalAge = true;

	let authorization1 = isAdmin && isRegistered; // f && t = false
	console.log (authorization1);

	let voteAuthorization = isLegalAge && isRegistered;
	console.log(voteAuthorization);

	let adminAccess = isAdmin && isLegalAge && isRegistered;
	console.log(adminAccess);// result: false


	let random = isAdmin && false;
	console.log(random);

	let requiredLevel = 95;
	let requiredAge = 18;

	let gameTopPlayer = isRegistered && requiredLevel === 25;
	console.log(gameTopPlayer);

	let gamePlayer = isRegistered && isLegalAge && requiredLevel >=25;
	console.log(gamePlayer); 


	let userName = 'kookie2022';
	let userName2 = 'gamer01'
	let userAge = 15;
	let userAge2 = 26;


	let registration1 = userName.length > 8 && userAge >= requiredAge;
	console.log(registration1);

	let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
	console.log(registration2);



	//for OR operator

	let userLevel = 100;
	let userLevel2 = 65;

	let guildRequirement = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge;
	console.log(guildRequirement);

	let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin);

	// Not operator ! symbol
	let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
	console.log(guildAdmin2);